const baseUrl = "https://api.ipify.org/?format=json"
const url = "http://ip-api.com"
const div = document.getElementById("root")
const btn = document.createElement("button")
btn.textContent= "Знайти по IP"
div.append(btn)
btn.addEventListener("click",async ()=>{
   const IP = await getData(baseUrl)
   const asdData = await getDataIp(url,IP)
   renderData(asdData)
})
async function  getData(url){
  const res = await fetch(url)
  const data = await res.json()
  return data.ip
}
async function getDataIp(url,IP){
    const res = await fetch(`${url}/json/${IP}?fields=continent,country,region,city`)
    const data = await res.json()
    return data
}

async function renderData(asdData){
div.insertAdjacentHTML("beforeend",`<p>${asdData.continent}</p><p>${asdData.country}</p><p>${asdData.region}</p><p>${asdData.city}</p>`)
}

